<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta charset="UTF-8">
        <meta name="description" content="The hompage of MPS">
        <title>mps team</title>
        <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.9.95/css/materialdesignicons.min.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap"/>
        <link rel="stylesheet" href="../ASSETS/styles.css"/>
        <link rel="stylesheet" href="../ASSETS/sidebar.css"/>
        <link rel="stylesheet" href="../ASSETS/discord.css"/>
    </head>
    <body>
        <aside class="sidebar">
            <nav>
                <ul class="sidebar__nav">
                    <li>
                        <a href="../DISCORD/discord-home.php" class="sidebar__nav__link">
                            <i class="mdi mdi-discord"></i>
                            <span class="sidebar__nav__text">Discord</span>
                        </a>
                    </li>
					<li>
						<a href="../SERVERS/servers-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-server"></i>
							<span class="sidebar__nav__text">Servers</span>
						</a>
					</li>
					<li>
						<a href="../MPSTEAM/mpsteam-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-account-group"></i>
							<span class="sidebar__nav__text">MPS team</span>
						</a>
					</li>                    
					<li>
						<a href="../index.php" class="sidebar__nav__link">
							<i class="mdi mdi-home"></i>
							<span class="sidebar__nav__text">Home</span>
						</a>
					</li>
                </ul>
            </nav>
        </aside>

        <main class="main">
            <section>
                <header>
                    <h1 style="color: black;">MPS team</h1>
                    <button onclick="history.back()">Go Back</button>
                    <hr>
                    <h2 id="Discord">Discord MPS team</h2>
                    <hr>
                </header>
                <body>
                    <h3>Owner</h3>
                    <section>
                        <div class="profile-card">
                            <img class="avatar" src="https://cdn.discordapp.com/widget-avatars/_dA0Y7ARqPwDfJh0CEeiURNG44shaEi1sp8dA8ujGjo/vy4gMOjs8NkqqRHYLn1OxKFL8KUfQ1YyIHlgjWTx_N3HCEihZgljFLLXMnGXnaQW5a9kNoiC8RL26drpYbiUkoq_CJv--Su0ytQ4ZRW58rj-p2d4K7vyX9LpTLaCBzSYt1A0ERfKaN_QjQ">
                            <br>
                            <span class="username">Nugget</span>
                            <br>
                            <p class="about">
                                Hello, I'm Nugget.
                                I am the creator of the MPS bots on the MPS Discord server as well as the maker of this website.
                                
                            </p>
                        </div>
                    </section>
                    <h3>Staff<hr></h3>
                    <section>
                        <h4>Admins</h4>
                        <div class="profile-card">
                            <img class="avatar" src="">
                            <br>
                            <span class="username">placeholder</span>
                            <br>
                            <p class="about">
                                placeholder
                            </p>
                        </div>
                        <div class="profile-card">
                            <img class="avatar" src="">
                            <br>
                            <span class="username">placeholder</span>
                            <br>
                            <p class="about">
                                placeholder
                            </p>
                        </div>
                    </section>
                </body>
                <footer>
                    <h5>Go back to the <a href="../index.html">home page</a></h5>
                </footer>  
            </section>
        </main>  
    </body>
</html>