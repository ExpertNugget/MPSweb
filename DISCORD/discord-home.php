<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta charset="UTF-8">
        <meta name="description" content="The hompage of MPS">
        <title>discord</title>
        <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.9.95/css/materialdesignicons.min.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap"/>
        <link rel="stylesheet" href="../ASSETS/styles.css"/>
        <link rel="stylesheet" href="../ASSETS/sidebar.css"/>
    </head>
    </head>
    <body>
        <aside class="sidebar">
            <nav>
                <ul class="sidebar__nav">
                    <li>
                        <a href="../DISCORD/discord-home.php" class="sidebar__nav__link">
                            <i class="mdi mdi-discord"></i>
                            <span class="sidebar__nav__text">Discord</span>
                        </a>
                    </li>
					<li>
						<a href="../SERVERS/servers-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-server"></i>
							<span class="sidebar__nav__text">Servers</span>
						</a>
					</li>
					<li>
						<a href="../MPSTEAM/mpsteam-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-account-group"></i>
							<span class="sidebar__nav__text">MPS team</span>
						</a>
					</li>                    
					<li>
						<a href="../index.php" class="sidebar__nav__link">
							<i class="mdi mdi-home"></i>
							<span class="sidebar__nav__text">Home</span>
						</a>
					</li>
                </ul>
            </nav>
        </aside>

        <main class="main">
            <header>
                <h1 style="color: black;">Discord</h1>
                <button onclick="history.back()">Go Back</button>
                <hr>
            </header>
            <section>
                <h2> <a href="https://discord.gg/PwCrcXU" target="_blank" >MPS Discord server.</a></h2>
                <iframe src="https://discord.com/widget?id=357641050707329035&theme=dark" width="350" height="500" allowtransparency="true" frameborder="0" sandbox="allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts"></iframe>
            </section>
            <section>
                <h2><a href="../DISCORD/bots.php">Custom Discord bots.</a></h2>
                <p>
                    These are the bots that are custom made for the MPS Discord server.
                </p>
            </section>
            <section>
                <h2><a href="../MPSTEAM/mpsteam-home.html#Discord">Discord team.</a></h2>
            </section>
            <footer>
                <h5>Go back to the <a href="../index.php:">home page</a></h5>
            </footer>  
        </main>  

    </body>
</html>