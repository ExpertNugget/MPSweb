<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta charset="UTF-8">
        <meta name="description" content="The hompage of MPS">
        <title>discord bots</title>
        <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.9.95/css/materialdesignicons.min.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap"/>
        <link rel="stylesheet" href="../ASSETS/styles.css"/>
        <link rel="stylesheet" href="../ASSETS/sidebar.css"/>
    </head>
    <body>
        <aside class="sidebar">
            <nav>
                <ul class="sidebar__nav">
                    <li>
                        <a href="../DISCORD/discord-home.php" class="sidebar__nav__link">
                            <i class="mdi mdi-discord"></i>
                            <span class="sidebar__nav__text">Discord</span>
                        </a>
                    </li>
					<li>
						<a href="../SERVERS/servers-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-server"></i>
							<span class="sidebar__nav__text">Servers</span>
						</a>
					</li>
					<li>
						<a href="../MPSTEAM/mpsteam-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-account-group"></i>
							<span class="sidebar__nav__text">MPS team</span>
						</a>
					</li>                    
					<li>
						<a href="../index.php" class="sidebar__nav__link">
							<i class="mdi mdi-home"></i>
							<span class="sidebar__nav__text">Home</span>
						</a>
					</li>
                </ul>
            </nav>
        </aside>
        <main class="main">
            <header>
                <h1>MPS's custom bots</h1>
                <button onclick="history.back()">Go Back</button>
                <hr>
            </header>
            <section>
                <h2><a href="https://github.com/ExpertNugget/MPS" target="_blank">MPS</a></h2>
                <p>
                    This is the MPS discord bot.<br/>
                    This bot is ran with <a href="https://github.com/Cog-Creators/Red-DiscordBot" target="_blank">RedBot</a><br/>
                    ...More info will be added soon!
                </p>
            </section>
            <section>
                <h2><a href="https://github.com/ExpertNugget/MPS-rewrite" target="_blank">MPS-RW</a></h2>
                <p>
                    This is a much newer and relatively less feature rich bot compared to MPS.<br/>
                    This bot is ran with <a href="https://pycord.dev/" target="_blank">PyCord</a>.
                </p>
            </section>
        </main>
    </body>
</html>