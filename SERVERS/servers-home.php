<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta charset="UTF-8">
        <meta name="description" content="The hompage of MPS">
        <title>servers</title>
        <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.9.95/css/materialdesignicons.min.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap"/>
        <link rel="stylesheet" href="../ASSETS/styles.css"/>
        <link rel="stylesheet" href="../ASSETS/sidebar.css"/>
    </head>
    <body>
        <aside class="sidebar">
            <nav>
                <ul class="sidebar__nav">
                    <li>
                        <a href="../DISCORD/discord-home.php" class="sidebar__nav__link">
                            <i class="mdi mdi-discord"></i>
                            <span class="sidebar__nav__text">Discord</span>
                        </a>
                    </li>
					<li>
						<a href="../SERVERS/servers-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-server"></i>
							<span class="sidebar__nav__text">Servers</span>
						</a>
					</li>
					<li>
						<a href="../MPSTEAM/mpsteam-home.php" class="sidebar__nav__link">
							<i class="mdi mdi-account-group"></i>
							<span class="sidebar__nav__text">MPS team</span>
						</a>
					</li>                    
					<li>
						<a href="../index.php" class="sidebar__nav__link">
							<i class="mdi mdi-home"></i>
							<span class="sidebar__nav__text">Home</span>
						</a>
					</li>
                </ul>
            </nav>
        </aside>

        <main class="main">
            <header>
                <h1 style="color: black;">MPS game servers.</h1>
                <button onclick="history.back()">Go Back</button>
                <hr>
            </header>
            <section>
                <h2>mps.modded.fun</h2>
                <p>
                    This is a modded Minecraft server running 1.18.1 on the <a href="https://www.curseforge.com/minecraft/modpacks/mps-s7">MPS S7</a> modpack.
                </p>
            </section>
        </main>
    </body>
</html>