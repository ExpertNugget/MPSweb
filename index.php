<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width"> <!-- Fits to mobile screens. -->
        <meta charset="UTF-8">
        <meta name="description" content="The hompage of MPS">
        <title>home</title>
        <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.9.95/css/materialdesignicons.min.css"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap"/>
        <link rel="stylesheet" href="./ASSETS/styles.css"/>
        <link rel="stylesheet" href="./ASSETS/sidebar.css"/>
        <link rel="icon" type="image/x-icon" href="https://cdn.discordapp.com/attachments/859710530884337684/932704628871676015/MPS13.png"/>
    </head>
        <body>
            <aside class="sidebar">
                <nav>
                    <ul class="sidebar__nav">
                        <li>
                            <a href="./DISCORD/discord-home.php" class="sidebar__nav__link">
                                <i class="mdi mdi-discord"></i>
                                <span class="sidebar__nav__text">Discord</span>
                            </a>
                        </li>
                        <li>
                            <a href="./SERVERS/servers-home.php" class="sidebar__nav__link">
                                <i class="mdi mdi-server"></i>
                                <span class="sidebar__nav__text">Servers</span>
                            </a>
                        </li>
                        <li>
                            <a href="./MPSTEAM/mpsteam-home.php" class="sidebar__nav__link">
                                <i class="mdi mdi-account-group"></i>
                                <span class="sidebar__nav__text">MPS team</span>
                            </a>
                        </li>                    
                        <li>
                            <a href="./index.php" class="sidebar__nav__link">
                                <i class="mdi mdi-home"></i>
                                <span class="sidebar__nav__text">Home</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </aside>
            <main class="main">
                <header>
                    <h1 style="color: black;">Welcome to the begining of the MPS Website!</h1>
                    <hr>
                </header>
                <main>
                    <section>
                        <p>
                            This is the home page of the MPS website.<br>
                            Theres not much here now but its a start.
                        </p>
                    </section>
                </main>
                <footer>
                    <h5 style="color: black;">Best website of 2022 says me</h5>
                    <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ" target="_black">Since your down here check out this link!</a>
                </footer>
            </main>
        </body>
</html>